$(document).ready(function(){
    $("#SearchField").on("keyup", function() {
        var q = $(this).val();
        $.ajax({
            url: 'caricari/',
            data:{
                'q':q
            },
            datatype:'json',
            success: function(data) {
                if(q.length==0){
                    $('#content-tabel').hide();
                    $('#content').hide();
                }  
                else{
                    $('#content-tabel').html('')
                    var result =
                    '<tr>'
                    +'<td>'+data[0].fields.positif+ '</td>' 
                    +'<td>'+data[0].fields.meninggal+ '</td>'
                    +'<td>'+data[0].fields.sembuh+ '</td>'
                    +'<td>'+data[0].fields.kasusAktif+ '</td>'
                    +'<td>'+data[0].fields.tanggal+ '</td>'
                    +'<td>'+data[0].fields.nama+ '</td>'
                    +'</tr>'
                    $('#content-tabel').append(result);
                    $('#content').show();
                    $('#kontn').hide();
                    $('#content-tabel').show();
                }
            }
        });
    });

});
