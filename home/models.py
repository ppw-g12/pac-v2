from django.db import models

# Create your models here.

from django.db import models

# Create your models here.
class UpdateKasus(models.Model):
    positif = models.IntegerField()
    kasusAktif = models.IntegerField()
    meninggal = models.IntegerField()
    sembuh = models.IntegerField()
    tanggal = models.DateField()
    nama = models.TextField(max_length=100, blank=True, default='')
