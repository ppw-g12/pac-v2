from django.shortcuts import render, redirect
from .forms import UpdateKasusForm
from .models import UpdateKasus
from userlogin import views
import json
from django.core import serializers
from django.http import JsonResponse, HttpResponse


# Create your views here.

def create(request):
    if request.method == "POST":
        form = UpdateKasusForm(request.POST)
        if form.is_valid():
            kasus = UpdateKasus()
            kasus.nama = form.cleaned_data['nama']
            kasus.positif = form.cleaned_data['positif']
            kasus.kasusAktif = form.cleaned_data['kasusAktif']
            kasus.meninggal = form.cleaned_data['meninggal']
            kasus.sembuh = form.cleaned_data['sembuh']
            kasus.tanggal = form.cleaned_data['tanggal']
            kasus.save()
        return redirect('/daftar')
    else:
        data = UpdateKasus.objects.all()
        form = UpdateKasusForm()
        response = {"kasus":data,'form':form}
        return render(request,'home/daftarkasus.html', response)

def index(request):
    data = UpdateKasus.objects.all()
    response = {"kasus":data}
    return render(request, 'home/index.html', response)

def daftar(request):
    data = UpdateKasus.objects.all()
    response = {"kasus":data}
    return render(request, 'home/listkasus.html', response)

def searchkasus(request):
    q = request.GET.get('q')
    nama= UpdateKasus.objects.filter(nama__icontains = q)
    data = serializers.serialize('json', nama)
    return HttpResponse(data, content_type="text/json-comment-filtered")
 