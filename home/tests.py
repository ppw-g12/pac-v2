from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import index, searchkasus, create, daftar
from django.apps import apps
from home.apps import HomeConfig
from .models import UpdateKasus

# Create your tests here.

# URL Testcases
class UrlsTest(TestCase):
    def test_url_exist_home(self):
        response = Client().get('/')

# View Testcases 
class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.test_subject = UpdateKasus.objects.create(
        positif = 1000,
        kasusAktif = 2000,
        meninggal = 3000,
        sembuh = 4000,
        tanggal = "2000-12-12",
        nama = "nurul")

    def test_view_home_is_using_function_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_view_home_is_using_function_create(self):
        found = resolve('/update/')
        self.assertEqual(found.func, create)
    
    def test_view_home_is_using_function_daftar(self):
        found = resolve('/daftar/')
        self.assertEqual(found.func, daftar)

    def test_positif_subject(self):
        self.assertEqual(str(self.test_subject.positif), "1000")
    def test_aktif_subject(self):
        self.assertEqual(str(self.test_subject.kasusAktif), "2000")
    def test_mninggoy_subject(self):
        self.assertEqual(str(self.test_subject.meninggal), "3000")
    def test_smbuh_subject(self):
        self.assertEqual(str(self.test_subject.sembuh), "4000")
    def test_tanggal_subject(self):
        self.assertEqual(str(self.test_subject.tanggal), "2000-12-12")
    def test_nama_subject(self):
        self.assertEqual(str(self.test_subject.nama), "nurul")

# Template Testcases
class TemplatesTest(TestCase):
    def test_template_home_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, "home/index.html")
    def test_template_home_using_daftarasus_template(self):
        response = Client().get('/update/')
        self.assertTemplateUsed(response, "home/daftarkasus.html")
    def test_template_home_using_list_template(self):
        response = Client().get('/daftar/')
        self.assertTemplateUsed(response, "home/listkasus.html")
    def test_apps(self):
        self.assertEqual(HomeConfig.name, 'home')
        self.assertEqual(apps.get_app_config('home').name, 'home')





