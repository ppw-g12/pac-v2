from django import forms
from .models import UpdateKasus

class UpdateKasusForm(forms.ModelForm):
  class Meta:
    model = UpdateKasus
    fields = [
      'positif',
      'kasusAktif',
      'meninggal',
      'sembuh',
      'tanggal',
      'nama',
    ]
    widgets = {
      'positif': forms.TextInput(attrs={'class': 'form-control'}),
      'kasusAktif': forms.TextInput(attrs={'class': 'form-control'}),
      'meninggal': forms.TextInput(attrs={'class': 'form-control'}),
      'sembuh': forms.TextInput(attrs={'class': 'form-control'}),
      'tanggal' : forms.DateInput(format='%m/%d/%Y', attrs={'class':'form-control', 'placeholder':'Select a date', 'type':'date'}),
      'nama': forms.TextInput(attrs={'class': 'form-control'}),
    }
