from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('update/', views.create, name='create'),
    path('daftar/', views.daftar, name='create'),
    path('daftar/caricari/', views.searchkasus, name='searchkasus')
]
