# Generated by Django 3.1.1 on 2021-01-02 07:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_updatekasus_nama'),
    ]

    operations = [
        migrations.AlterField(
            model_name='updatekasus',
            name='nama',
            field=models.TextField(blank=True, default='', max_length=100),
        ),
    ]
