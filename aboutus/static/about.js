$(document).ready(function(){
    $("#SearchIn").on("keyup", function() {
        var q = $(this).val();
        $.ajax({
            url: 'cariKritik/',
            data:{
                'q':q
            },
            datatype:'json',
            success: function(data) {
                if(q.length==0){
                    $('#content-tabel').hide();
                    $('#content').hide();
                }  
                else{
                    $('#content-tabel').html('')
                    var result =
                    '<tr>'
                    +'<td>'+data[0].fields.Username+ '</td>' 
                    +'<td>'+data[0].fields.Date+ '</td>'
                    +'<td>'+data[0].fields.Pesan+ '</td>'
                    +'</tr>'
                    $('#content-tabel').append(result);
                    $('#content').show();
                    $('#kontn').hide();
                    $('#content-tabel').show();
                }
            }
        });
    });

});
