from django import forms
from .models import Kritik

class KritikForm(forms.Form):
    Username = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Anda',
        'type' : 'text',
        'required' : True,
    }))

    Date = forms.DateField(widget=forms.DateInput(attrs={
        'class' : 'datepicker',
        'type' : 'date',
        'required' : True,
    }))

    Pesan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Pesan Anda',
        'type' : 'text',
        'required' : True,
    }))