from django.contrib import admin
from django.urls import include, path
from . import views
from django.views.decorators.csrf import csrf_exempt
from .views import cariKritik


app_name = 'aboutus'

urlpatterns = [
    path('', views.about, name="about"),
    path('Kritik/', views.addKritik, name="kritik"),
    path('cariKritik/', csrf_exempt(views.cariKritik), name="cariKritik")
]
