from django.shortcuts import render, redirect
from .models import Artikel, Komentar
from .forms import FormArtikel, FormKomentar
from django.contrib import messages
from userlogin import views
import json
from django.http import JsonResponse


# Create your views here.


def artikel(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            formA = FormArtikel(request.POST, request.FILES)
            formB = FormKomentar(request.POST)

            if formA.is_valid():
                formA.save()
                messages.success(request, (f"Artikel {request.POST['judul']} berhasil ditambahkan!"))
                return redirect ('artikel:artikel')
            
            else :
                messages.warning(request, (f"input tidak sesuai!"))
                return redirect('artikel:artikel')
        
        else:
            formA = FormArtikel()
            formB = FormKomentar()
            listArtikel = Artikel.objects.all()
            listKomen = Komentar.objects.all()
            context = {
                'formA' : formA,
                'formB' : formB,
                'listArtikel' : listArtikel,
                'listKomen' : listKomen,
            }
            return render(request, 'artikel/index.html', context)
    
    else:
        listArtikel = Artikel.objects.all()
        listKomen = Komentar.objects.all()
        context = {
            'listArtikel' : listArtikel,
            'listKomen' : listKomen,
        }
        return render(request, 'artikel/index.html', context)



def detail(request, pk):
    artikel = Artikel.objects.get(id=pk)
    listKomen = Komentar.objects.all()

    if request.user.is_authenticated:
        context = {
        'artikel' : artikel,
        'listKomen' : listKomen,
    }

    else : 
        context = {
            'artikel' : artikel,
        }

    if request.method == "POST" :
        formB = FormKomentar(request.POST)

        if formB.is_valid() :
            formB.save()
            messages.success(request, (f"Komentar berhasil ditambahkan!"))
            return redirect('artikel:detail', pk=artikel.pk)
        
        else :
            messages.warning(request, (f"Input tidak sesuai!"))
            return redirect ('artikel:detail', pk=artikel.pk)
    
    else :
        artikel = Artikel.objects.get(id=pk)
        listKomen = Komentar.objects.filter(artikel = artikel)
        formB = FormKomentar(request.POST)
        namaLengkap = request.user.get_username()
        
        context = {
            'artikel' : artikel,
            'formB' : formB,
            'listKomen' : listKomen,
            'namaLengkap' : namaLengkap,
        }
        return render(request, 'artikel/artikel-detail.html', context)


def buatArtikel(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            formA = FormArtikel(request.POST, request.FILES)
            formB = FormKomentar(request.POST)

            if formA.is_valid():
                formA.save()
                messages.success(request, (f"Artikel {request.POST['judul']} berhasil ditambahkan!"))
                return redirect ('artikel:buatArtikel')
            
            else :
                messages.warning(request, (f"input tidak sesuai!"))
                return redirect('artikel:buatArtikel')
        
        else:
            formA = FormArtikel()
            formB = FormKomentar()
            context = {
                'formA' : formA,
                'formB' : formB,
            }
            return render(request, 'artikel/artikel-baru.html', context)


def delete(request, pk):
    artikel = Artikel.objects.get(id=pk)
    artikel.delete()
    # messages.success(request, (f"Artikel {request.GET['artikel']} berhasil dihapus!"))
    messages.success(request, (f"Artikel berhasil dihapus!"))
    return redirect('artikel:artikel')

def deleteKomentar(request,pk):
    komentar = Komentar.objects.get(id=pk)
    Komentar.delete()
    messages.warning(request, (f"{komentar} berhasil dihapus"))
    return redirect('artikel:artikel')


def cariArtikel(request):
    if request.method == 'POST':
        search_str = json.loads(request.body).get('searchText')
        artikels = Artikel.objects.filter(
            judul__istartswith = search_str) | Artikel.objects.filter(
            penulis__istartswith = search_str)

            # judul__istartswith = search_str) | Artikel.objects.filter(
            # penulis__istartswith = search_str) | Artikel.objects.filter(
            # konten__istartswith = search_str)
            
        data = artikels.values()

        # print(data[0].gambar.url)

        return JsonResponse(list(data), safe=False)
