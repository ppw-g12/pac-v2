from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from . import views
from .views import cariArtikel


app_name = 'artikel'


urlpatterns = [
    path('', views.artikel, name='artikel'),
    path('/buatArtikel', views.buatArtikel, name='buatArtikel'),
    path('detail/<int:pk>/', views.detail, name='detail'),
    path('delete/<str:pk>', views.delete, name='delete'),
    path('delete/komentar/<str:pk>', views.deleteKomentar, name='komentarDelete'),
    path('/cariArtikel', csrf_exempt(views.cariArtikel), name='cariArtikel'),
]