from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Artikel, Komentar
from .views import artikel, detail, delete, buatArtikel, deleteKomentar


# Create your tests here.

# class TestViews(TestCase):
#     def setUp(self):
#         self.client = Client()
#         self.artikel_url = reverse('artikel')
#         self.test_artikel = Artikel.objects.create(
#             artikel = 'Bahaya Indomie'
#         )
#         self.test_komentar = Komentar.objects.create(
#             komentar = 'Mantap sehat',
#             article = self.test_artikel
#         )
#         self.test_delete_url = reverse("artikel:artikelDelete", args=[self.test_artikel.id])
#         self.test_komentar_delete_url = reverse("home:komentarDelete", args=[self.test_komentar.id])

#         def test_artikel_GET(self):
#             response = self.client.get(self.artikel_url)
#             self.assertEquals(response.status_code, 200)
#             self.assertTemplateUsed(response, "artikel/index.html")
        
#         def test_artikel_POST(self):
#             response = self.client.post(self.artikel_url, {
#                 "artikel" : "naruto"
#             }, follow=True)
#             self.assertContains(response, "naruto")

#         def test_komentar_POST(self):
#             response = self.client.post(self.artikel_url, {
#                 "komentar" : "keren",
#                 "article" : "self.test_artikel.id",
#             }, follow=True)
#             self.assertContains(response, "Hadi")

#         def test_notValid_POST(self):
#             response = self.client.post(self.artikel_url, {
#                 "komentar" : "keren",
#                 "article" : "hihi",
#             }, follow=True)
#             self.assertContains(response, "Input tidak sesuai")

#         def test_komentar_DELETE(self):
#             response = self.client.get(self.test_komentar_delete_url, follow=True)
#             print(response.content)
#             self.assertContains(response, "berhasil dihapus")

#         def test_artikel_DELETE(self):
#             response = self.client.get(self.test_delete_url, follow=True)
#             self.assertContains(response, "berhasil dihapus")


class ArtikelTest(TestCase):
    def test_model_can_create_article(self):
        Artikel.objects.create(
            judul="corona",
            penulis="coronamenular",
            gambar="lalalala.jpeg",
            konten="content",
        )

    def test_event_url_is_exist(self):
        response = Client().get('/artikel')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/artikel')
        self.assertEqual(found_func.func, artikel)

    def test_event_using_template(self):
        template = Client().get('/artikel')
        self.assertTemplateUsed(template, 'artikel/index.html')

    def test_artikel_GET(self):
        response = self.client.get('/artikel')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "artikel/index.html")

class BuatArtikelTest(TestCase):
    # def test_event_url_is_exist(self):
    #     response = Client().get('/artikel/buatArtikel')
    #     self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/artikel/buatArtikel')
        self.assertEqual(found_func.func, buatArtikel)

    # def test_event_using_template(self):
    #     template = Client().get('/artikel/buatArtikel')
    #     self.assertTemplateUsed(template, 'artikel/artikel-baru.html')

    # def test_artikel_GET(self):
    #     response = self.client.get('/artikel/buatArtikel')
    #     self.assertEquals(response.status_code, 200)
    #     self.assertTemplateUsed(response, "artikel/artikel-baru.html")



# class KomentarTest(TestCase):
#     def test_komentar_POST(self):
#         response = self.client.post(reverse('artikel'), {
#             "komentar" : "keren",
#             "article" : "self.test_artikel.id",
#         }, follow=True)
#         self.assertContains(response, "Hadi")


    # def test_artikel_DELETE(self):
    #     response = self.client.get("artikel:artikelDelete", args=Artikel.objects.create(
    #          artikel = 'Bahaya Indomie'
    #      ), follow=True)
    #     self.assertContains(response, "berhasil dihapus")

    #  def setUp(self):
#         self.client = Client()
#         self.artikel_url = reverse('artikel')
#         self.test_artikel = Artikel.objects.create(
#             artikel = 'Bahaya Indomie'
#         )
#         self.test_komentar = Komentar.objects.create(
#             komentar = 'Mantap sehat',
#             article = self.test_artikel
#         )



# class DetailTest(TestCase):
#     def test_event_url_is_exist(self):
#         response = Client().get('/artikel/signup/')
#         self.assertEqual(response.status_code, 200)

#     def test_event_func(self):
#         found_func = resolve('/userlogin/signup/')
#         self.assertEqual(found_func.func, signup)

#     def test_event_using_template(self):
#         template = Client().get('/userlogin/signup/')
#         self.assertTemplateUsed(template, 'userlogin/signup.html')


# class SignOutTest(TestCase):
#     def test_event_url_is_exist(self):
#         response = Client().get('/userlogin/signout/')
#         self.assertEqual(response.status_code, 302)

#     def test_event_func(self):
#         found_func = resolve('/userlogin/signout/')
#         self.assertEqual(found_func.func, signout)
