from django import forms
from .models import Artikel, Komentar

class FormArtikel(forms.ModelForm):
    class Meta:
        model = Artikel
        fields = '__all__'
        widgets = {
            'judul' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Apa judul artikel anda?"
            }),

            'penulis' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Siapa nama anda?"
            }),

            'gambar': forms.FileInput(
                attrs = {
                    'class': 'form-control-file dropzone',
                }),

            'konten' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Silakan tulis konten artikel anda di sini"
            }),
        }

class FormKomentar(forms.ModelForm):
    class Meta:
        model = Komentar
        fields = '__all__' 
        widgets = {
            # 'nama' : forms.TextInput(attrs={
            #     'class': 'form-control',
            #     'placeholder': "Siapa nama anda?"
            # }),

            'komen' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Silakan tulis komentar anda di sini"
            }),
        }
