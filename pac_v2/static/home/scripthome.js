const searchField = document.querySelector("#searchField");
const tableOutput = document.querySelector(".table-output");
const noResult = document.querySelector(".no-result");
const fullTabelOutput = document.querySelector(".fullTabel");
const isiTabel = document.querySelector("#content-tabel");
tableOutput.style.display='none';
noResult.style.display='none';

searchField.addEventListener("keyup" , (e)=> {
  const searchValue = e.target.value;

  if(searchValue.trim().length > 0){
    console.log("searchValue", searchValue);
    isiTabel.innerHTML = "";

  fetch("/caricari",{
    body: JSON.stringify({ searchText: searchValue }),
    method: "POST",
  })
    .then((res) => res.json())
    .then((data) => {
      console.log('data', data);
      fullTabelOutput.style.display= "none";
      tableOutput.style.display="block";

      if(data.length === 0 ){
        noResult.style.display= "none";
        tableOutput.style.display= "none";
      }
      else{
        noResult.style.display= "none";
        data.forEach((UpdateKasus) => {
          isiTabel.innerHTML += `

              <tr>
              <th scope="row">${{UpdateKasus.positif}}</th>
              <td>${{xx.sembuh}}</td>
              <td>${{xx.meninggal}}</td>
              <td>${{xx.kasusAktif}}</td>
              <td>${{xx.tanggal}}</td>
              <td>${{xx.nama}}</td>
              </tr>
          `;
        })
      }
    })
  }else{
    tableOutput.style.display = "none";
    fullTabelOutput.style.display= "block";
  }
});
