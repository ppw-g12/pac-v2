const searchField = document.querySelector("#searchField");
const tableOutput = document.querySelector(".table-output");
const noResult = document.querySelector(".no-result");
const fullArtikelOutput = document.querySelector(".fullArtikel");
const isiArtikel = document.querySelector("#content-artikel");
tableOutput.style.display='none';
noResult.style.display='none';

searchField.addEventListener("keyup" , (e)=> {
  const searchValue = e.target.value;

  if(searchValue.trim().length > 0){
    console.log("searchValue", searchValue);
    isiArtikel.innerHTML = "";

  fetch("/artikel/cariArtikel",{
    body: JSON.stringify({ searchText: searchValue }),
    method: "POST",
  })
    .then((res) => res.json())
    .then((data) => {
      console.log('data', data);
      fullArtikelOutput.style.display= "none";
      tableOutput.style.display="block";

      if(data.length === 0 ){
        noResult.style.display= "block";
        tableOutput.style.display= "none";
      }
      else{
        noResult.style.display= "none";
        data.forEach((artikel) => {
          isiArtikel.innerHTML += `

      <div class="card" style="width: 18rem;">
            <a href="{% url 'artikel:delete' ${ artikel.id } %}" class="col-2">
                <button type="button" class="close mb-2">&times;</button>
            </a>
            <img src="/media/${artikel.gambar}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${artikel.judul}</h5>
                <p class="card-text"> ${ artikel.konten.slice(0,100)}  ...</p>
                <a href="{% url 'artikel:detail' ${ artikel.id } %}" class="btn btn-primary">Baca Selengkapnya!</a>
            </div>
        </div>
          `;
        })
      }
    })
  }else{
    tableOutput.style.display = "none";
    fullArtikelOutput.style.display= "block";
  }
});