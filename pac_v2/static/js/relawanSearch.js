const searchField = document.querySelector("#searchField");
const tableOutput = document.querySelector(".table-output");
const noResult = document.querySelector(".no-result");
const fullRelawanOutput = document.querySelector(".fullRelawan");
const isiRelawan = document.querySelector("#content-relawan");
tableOutput.style.display='none';
noResult.style.display='none';

searchField.addEventListener("keyup" , (e)=> {
  const searchValue = e.target.value;

  if(searchValue.trim().length > 0){
    console.log("searchValue", searchValue);
    isiRelawan.innerHTML = "";

  fetch("/relawan/search_relawan",{
    body: JSON.stringify({ searchText: searchValue }),
    method: "POST",
  })
    .then((res) => res.json())
    .then((data) => {
      console.log('data', data);
      fullRelawanOutput.style.display= "none";
      tableOutput.style.display="block";

      if(data.length === 0 ){
        noResult.style.display= "block";
        tableOutput.style.display= "none";
      }
      else{
        noResult.style.display= "none";
        data.forEach((relawan) => {
          isiRelawan.innerHTML += `
          <div class="p ml-3 mb-3">
        <div class="card" style="width: 22em;">
          <div class="card-body">
            <h5 class="card-title">${relawan.nama}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${relawan.email}</h6>
            <p class="card-text">${relawan.alasan}</p>
            <a href="#" class="card-link text-primary" data-toggle="modal" data-target="#${ relawan.id }">Lebih Lanjut</a>
          </div>
        </div>
        <div class="modal fade" id="${ relawan.id }" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title text-danger" id="exampleModalLabel">${relawan.nama}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>SWOT:</p>
                <p class='deskripsi-box'>${relawan.swot}</p>
                <p>Minat:</p>
                <p class='deskripsi-box'>${relawan.tertarik}</p>
                <p>Alasan ikut Relawan:</p>
                <p class='deskripsi-box'>${relawan.alasan}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
          `;
        })
      }
    })
  }else{
    tableOutput.style.display = "none";
    fullRelawanOutput.style.display= "block";
  }
});