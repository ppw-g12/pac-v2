$( document ).ready(function() {
    $( "#accordion" ).accordion({
        collapsible: true,
        heightStyle: "content"
    });

    $( "#time-button" ).click(function(event) {
        event.getElementById('demo').innerHTML = Date();
    });

    $( "img.img-responsive" ).mouseover(function() {
        var $parent = $(this).style.height = "175px";
        return false;
    });

    $( "img.img-responsive" ).mouseout(function() {
        var $parent = $(this).style.height = "125px";
        return false;
    });

    $( "table" ).keypress(function(event) {
        event.getElementById("input-press").style.backgroundColor = "red";
    });
    
});