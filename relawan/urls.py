from django.urls import path
from . import views
from .views import DaftarRelawan, ListRelawan, HapusRelawan
from django.views.decorators.csrf import csrf_exempt

app_name = 'relawan'

urlpatterns = [
  path('', ListRelawan.as_view(), name='List-relawan'),
  path('Daftar', DaftarRelawan.as_view(), name='Daftar-relawan'),
  path('Hapus/<int:pk>', HapusRelawan.as_view(), name='Hapus-relawan'),
  path('search_relawan', csrf_exempt(views.search_relawan), name = "search_relawan"),
]