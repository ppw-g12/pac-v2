const searchField = document.querySelector("#searchField");
const tableOutput = document.querySelector(".table-output");
const noResult = document.querySelector(".no-result");
const fullRelawanOutput = document.querySelector(".fullRelawan");
tableOutput.style.display='none';
noResult.style.display='none';
var jumlahRelawan = 0;

//Event 1 : Keyup searchbar using AJAX
$("#searchField").keyup(function() {
  var isi_keyword = $("#searchField").val();
  console.log(isi_keyword);
  search_relawan(isi_keyword)
})

function search_relawan(data) {
  $.ajax({
      async: true,
      url: "/relawan/search_relawan",
      data: JSON.stringify({ searchText: data }),
      type: "POST",
      success: function (hasil) {
          console.log(hasil);
          fullRelawanOutput.style.display='none';
          var obj_hasil = $("#content-relawan");
          obj_hasil.empty();
          jumlahanRelawan = hasil.length;
          if(hasil.length > 0){
            tableOutput.style.display="block";
            noResult.style.display='none';
            for (i = 0; i < hasil.length; i++) {
              var tmp_nama = hasil[i].nama;
              var tmp_email = hasil[i].email;
              var tmp_alasan= hasil[i].alasan;
              var tmp_id = hasil[i].id;
              var tmp_swot = hasil[i].swot;
              var tmp_tertarik = hasil[i].tertarik;
              console.log(tmp_email);
              obj_hasil.append(
              `<div class="p ml-3 mb-3">
              <div class="card" style="width: 22em;">
                <div class="card-body">
                  <h5 class="card-title">`+ tmp_nama +`</h5>
                  <h6 class="card-subtitle mb-2 text-muted">`+ tmp_email +`</h6>
                  <p class="card-text">`+ tmp_alasan +`</p>
                  <a href="#" class="card-link text-primary" data-toggle="modal" data-target="#`+ tmp_id +`">Lebih Lanjut</a>
                </div>
              </div>
              <div class="modal fade" id="`+ tmp_id +`" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-danger" id="exampleModalLabel">`+ tmp_nama +`</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>SWOT:</p>
                      <p class='deskripsi-box'>`+ tmp_swot +`</p>
                      <p>Minat:</p>
                      <p class='deskripsi-box'>`+ tmp_tertarik +`</p>
                      <p>Alasan ikut Relawan:</p>
                      <p class='deskripsi-box'>`+ tmp_alasan +`</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                `
                );
            }
        }
        else{
          tableOutput.style.display='none';
          noResult.style.display='block';
        }
      }
  })
}



//Event 2 : Auto Complete 
var availableTags = [
  "Anan",
  "Andhika",
  "Alfred",
  "Jahe",
  "Fikri",
];
$( "#searchField" ).autocomplete({
  source: availableTags
});

//Event 3 : Tagged Card
$( function() {
  $('.card').click( function() {
    $(this).css('background', '#06f1b7')
  } );
} );

//Event 4 : Border ketik
$("#searchField").keydown(function() {
  $(this).css("border", '4px solid red');
})

$("#searchField").keyup(function () {
  $(this).css("border", '4px solid #000000');
  var isi_keyword = $("#searchField").val();
  console.log(isi_keyword);
  cari(isi_keyword)
})

