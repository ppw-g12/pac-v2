from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, DeleteView
from .forms import RelawanForm
from .models import Relawan
from django.urls import reverse_lazy
#TK 2
import json
from django.http import JsonResponse

def search_relawan(request):
    if request.method == 'POST':
        search_str = json.loads(request.body).get('searchText')
        relawans = Relawan.objects.filter(
            nama__istartswith = search_str) | Relawan.objects.filter(
            telp__istartswith = search_str) | Relawan.objects.filter(
            email__istartswith = search_str)
            
        data = relawans.values()

        return JsonResponse(list(data), safe=False)

class DaftarRelawan(CreateView):
    model = Relawan
    form_class = RelawanForm
    template_name = 'daftar-relawan.html'

class ListRelawan(ListView):
    model = Relawan
    template_name = 'list-relawan.html'

class HapusRelawan(DeleteView):
    model = Relawan
    template_name = 'hapus-relawan.html'
    success_url = reverse_lazy('relawan:List-relawan')


