$( document ).ready(function() {
    $( "#accordion" ).accordion({
        collapsible: true,
        heightStyle: "content"
    });

    $( "#time-button" ).click(function() {
        var d = new Date();
        d.setDate(10);
        document.getElementById("demo").innerHTML = d;
    });

    $( "img.overout" ).hover(function() {
        $(this).animate({
        height: "100%"},
        'slow');}
        , function() {
            $(this).animate({
                height: "80%"
            }, 'slow');
    });
    
    $('#searchButton').click( function(){
        var key = $('#search').val();


    $.ajax({
        method: 'GET',
        url : `/hotline/getData/?key=${key}`,
        success: function(response) {
            console.log(response);
            $('#data').empty();
        
            var result = "";
            for (let i = 0; i < response.length; i++) {
                result += ('<tr>' + '<td>' + response[i].name + '</td>' + '<td>' + response[i].address + '</td>' + '<td>' + response[i].region + '</td>' + '<td>' + response[i].phone + '</td>' + '<td>' + response[i].province + '</td>')
            }
            console.log(result);
            $('#data').append(result);
            $('#data').append('</table>');
        }
    })
    })
});