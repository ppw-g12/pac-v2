from django.test import TestCase, Client
from django.urls import resolve
from .models import AddHotline
from .views import html_add_hotline, html_home_hotline
from .forms import FormHotline
from http import HTTPStatus
from django.contrib.auth.models import User

# Create your tests here.
class test_home_hotline(TestCase):
    def test_url_home_exist(self):
        response = Client().get('/hotline/')
        self.assertEqual(response.status_code,200)

    def test_views_home_hotline(self):
        found = resolve('/hotline/')           
        self.assertEqual(found.func, html_home_hotline)

    def test_jquery_hotline(self):
        response = Client().get('/hotline/')
        html_contents = response.content.decode("utf8")
        self.assertIn('<button type="button" class="button" onclick="" id="time-button">Click me to display Date and Time.</button>', html_contents)
 
class test_add_hotline(TestCase):
    def test_url_add_exist(self):
        response = Client().get('/hotline/addhotline')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/userlogin/signin/?next=/hotline/addhotline')

    def test_views_add_hotline(self):
        found = resolve('/hotline/addhotline')           
        self.assertEqual(found.func, html_add_hotline)

    def test_add_logged_in(self):
        response = Client().get('/hotline/addhotline/')
        user = User.objects.create_user(first_name='jahe', last_name='azzahra', username='inijahe', email='jahe@gmail.com', password='jaheeee1234')
        user.save()
        c = Client()
        c.login(username="inijahe", password="jaheeee1234") 
        response = c.get('/hotline/addhotline')
        self.assertEqual(response.status_code, 200)

    def test_ajax_addhotline(self):
        response = Client().get('/hotline/addhotline/')
        user = User.objects.create_user(first_name='jahe', last_name='azzahra', username='inijahe', email='jahe@gmail.com', password='jaheeee1234')
        user.save()
        c = Client()
        c.login(username="inijahe", password="jaheeee1234") 
        response = c.get('/hotline/addhotline')
        html_contents = response.content.decode("utf8")
        self.assertIn('<table class="table" id="dataTable" width="100%" cellspacing="0">', html_contents)

    def test_API_return_no_data(self):
        response = Client().get('/hotline/addhotline/')
        user = User.objects.create_user(first_name='jahe', last_name='azzahra', username='inijahe', email='jahe@gmail.com', password='jaheeee1234')
        user.save()
        c = Client()
        c.login(username="inijahe", password="jaheeee1234") 
        response = c.get('/hotline/getData/?key=Jakarta')
        self.assertEquals(response.status_code, HTTPStatus.OK)

 