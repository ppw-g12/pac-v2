#from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from .forms import FormHotline
from .models import AddHotline
from userlogin import views

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites import requests
from django.http import JsonResponse
import json
import requests

# Create your views here.
def html_home_hotline(request):
    listHotline = AddHotline.objects.all().order_by('nama')
    return render(request, 'hotline.html', {'list_hotline' : listHotline})

@login_required(login_url='userlogin:signin')
def html_add_hotline(request):
    #request.user.is_authenticated
    listHotline = FormHotline(request.POST or None)
    if (request.method == 'POST' and listHotline.is_valid()):
        listHotline.save()
        return redirect('/hotline/')
    return render(request, 'addhotline.html', {'tabel_form' : listHotline})

def search(data, key):
    listRS = []
    for items in data:
        if key in items["name"] or key in items["address"] or key in items["region"] or key in items["province"]:
            RSdict = {
                "name" : items["name"],
                "address" : items["address"],
                "phone" : items["phone"],
                "region" : items["region"],
                "province" : items["province"]
            }

            listRS.append(RSdict)
    if len(listRS) == 0:
        listRS.append({"error": "Maaf, rumah sakit tersebut tidak ada dalam daftar."})
    return listRS

def get_list_RS(request):
    key = request.GET.get('key')
    url = 'https://dekontaminasi.com/api/id/covid19/hospitals/'
    response = requests.get(url)
    data = json.loads(response.content)
    listJson = search(data, key)
    return JsonResponse(listJson, safe=False)